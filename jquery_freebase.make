; $Id$

core = 6.x

libraries[freebase_suggest_css][download][type] = "get"
libraries[freebase_suggest_css][download][url] = "http://freebaselibs.com/static/suggest/1.2.1/suggest.min.css"
libraries[freebase_suggest_css][directory_name] = "freebase_suggest"
libraries[freebase_suggest_css][destination] = "libraries"

libraries[freebase_suggest_js][download][type] = "get"
libraries[freebase_suggest_js][download][url] = "http://freebaselibs.com/static/suggest/1.2.1/suggest.min.js"
libraries[freebase_suggest_js][directory_name] = "freebase_suggest"
libraries[freebase_suggest_js][destination] = "libraries"

